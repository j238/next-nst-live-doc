# ライブ配信システム

## 1.機能
* リアルタイムビデオ配信(ライブ配信)

## 2.構成
```plantuml
@startuml
actor 配信者A
card アプリA
cloud GoogleCloud {
  agent GoogleAppEngine<<Node.js>>
  database Firebase
}
card アプリA1
card アプリA2
actor 視聴者A1
actor 視聴者A2
配信者A --> アプリA
アプリA -0)- Firebase: WebRTC
GoogleAppEngine -0)- Firebase: WebRTC
GoogleAppEngine -(0- アプリA1: HLS
GoogleAppEngine -(0- アプリA2: HLS
アプリA1 --> 視聴者A1
アプリA2 --> 視聴者A2
@enduml
```

## 3.コスト
サーバーコスト計算  
●データベース  
Firebase  
https://firebase.google.com/?hl=ja  
月額$25(約2800円)  
●配信サーバー(HLSサーバー)  
Google App Engine  
https://cloud.google.com/  
CPU:1コア メモリ:1.75GB  
ディスク:30GB OS:Ubuntu18.04LTS  
月額$27.16(約3000円)  
●CDN  
SD(480P)のバイトサイズ: 約1分25MB  
(参考URL)  
https://videcheki.com/%E5%8B%95%E7%94%BB-%E7%94%BB%E8%B3%AA-sd-hd-%E3%83%95%E3%83%ABhd/
https://digital-faq.olympus.co.jp/faq/public/app/servlet/qadoc?QID=003745  
25MB*225分*1000人*30日=168,750GB  
https://cloud.google.com/cdn/pricing?hl=ja  
10GB*$0.09+140GB*$0.06+18GB*$0.05=$10.2(約1140円)  
※ 配信中継(CDN)は受信者（ユーザー数）と配信の動画容量によって変わってきます。  
　例えば、SD(480P)だと 約1分25MBになります。  
　それが1日配信225分だとした場合、1ヶ月分(30日)をユーザー1000人に配信するとして、約1140円かかる計算です。  

